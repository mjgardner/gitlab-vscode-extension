{
  "extends": ["airbnb-base", "prettier"],
  "plugins": ["import"],
  "env": {
    "mocha": true,
    "jest": true
  },
  "settings": {
    "import/resolver": {
      "node": {
        "extensions": [".js", ".mjs", ".ts"]
      }
    }
  },
  "ignorePatterns": ["node_modules/", "webviews/", "out", "dist-*"],
  "rules": {
    "no-console": "off",
    "no-return-await": "off",
    "import/no-restricted-paths": [
      "error",
      {
        "zones": [
          {
            "target": "src/common/**",
            "from": ["src/desktop/**", "src/browser/**"],
            "message": "Code in src/common can't import desktop-specific or browser-specific code."
          }
        ]
      }
    ],
    "import/no-unresolved": [
      2,
      {
        // ignoring `graphql-request` due to a bug in eslint-plugin-import
        // https://github.com/import-js/eslint-plugin-import/issues/2703
        "ignore": ["vscode", "graphql-request"]
      }
    ],
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        "js": "never",
        "ts": "never"
      }
    ],
    "no-shadow": "warn",
    "no-use-before-define": ["warn", "nofunc"],
    "import/no-extraneous-dependencies": [
      "error",
      { "devDependencies": ["scripts/**/*", "**/*.test.js", "test/**/*"] }
    ],
    "no-restricted-syntax": [
      "error",
      {
        "selector": "ForInStatement",
        "message": "for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array."
      },
      {
        "selector": "LabeledStatement",
        "message": "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand."
      },
      {
        "selector": "WithStatement",
        "message": "`with` is disallowed in strict mode because it makes code impossible to predict and optimize."
      }
    ]
  },
  "reportUnusedDisableDirectives": true,
  "parserOptions": {
    // adding this to support import.meta
    // https://github.com/eslint/eslint/discussions/16037#discussioncomment-2998062
    "ecmaVersion": "latest"
  },
  "overrides": [
    {
      "files": ["**/*.ts"],
      "parser": "@typescript-eslint/parser",
      "parserOptions": {
        "project": "./tsconfig.json"
      },
      "extends": ["plugin:@typescript-eslint/recommended", "plugin:import/typescript", "prettier"],
      "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
      },
      "rules": {
        "@typescript-eslint/return-await": "error",
        "@typescript-eslint/parameter-properties": "error",
        "@typescript-eslint/no-floating-promises": "error",
        "@typescript-eslint/no-shadow": ["error"],
        "@typescript-eslint/no-unused-vars": ["error"],
        "import/extensions": [
          "error",
          "ignorePackages",
          {
            "ts": "never"
          }
        ],
        "import/prefer-default-export": "off",
        "import/no-extraneous-dependencies": [
          "error",
          { "devDependencies": ["**/*.test.ts", "test/**/*"] }
        ],
        "no-console": "error",
        "no-shadow": "off",
        "no-useless-constructor": "off",
        "no-unused-expressions": "off",
        "default-param-last": "off"
      },
      "settings": {
        "import/resolver": {
          "node": {
            "extensions": [".js", ".mjs", ".ts"]
          }
        }
      },
      "plugins": ["@typescript-eslint"]
    },
    {
      "files": ["**/*test.ts", "test/**/*.ts", "**/test_utils/**/*.ts"],
      "rules": {
        "@typescript-eslint/no-explicit-any": "warn"
      }
    }
  ]
}
